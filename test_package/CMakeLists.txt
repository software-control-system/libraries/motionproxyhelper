cmake_minimum_required(VERSION 3.15)
project(PackageTest CXX)

find_package(motionproxyhelper CONFIG REQUIRED)
find_package(yat CONFIG REQUIRED)

add_executable(test_package src/test_package.cpp)
target_link_libraries(test_package motionproxyhelper::motionproxyhelper)
target_link_libraries(test_package yat::yat)

