// ============================================================================
//
// = CONTEXT
//		Utility class - Implementation of a class encapsulating creation and control
//						of a Tango device proxy to a standard soleil motor
//
// = File
//		MotionProxyHelper.cpp
//
// = AUTHOR
//		Arafat NOUREDDINE - Synchrotron SOLEIL
//      F. Langlois - Synchrotron SOLEIL
//
// ============================================================================

#include <MotionProxyHelper.h>

namespace mph
{
//////////////////////////////////////////////////////////////////////////////////////
//-ctor
//////////////////////////////////////////////////////////////////////////////////////
	
MotionProxyHelper::MotionProxyHelper( 	const string sProxyName,
										const string sDescription,
										Tango::DeviceImpl * _device)
throw (Tango::DevFailed)
: Tango::LogAdapter((Tango::DeviceImpl *)_device)
{
	DEBUG_STREAM << "MotionProxyHelper::MotionProxyHelper():1 [ "<< sDescription<< " ] entering...  "  << endl;		
    yat::MutexLock scoped_lock(_lock);
	//Init. members
	_sProxyName				= sProxyName;
	_sDescription			= sDescription;
	_sAttributePositionName	= "position";
	_sAttributeBLSWName		= "backwardLimitSwitch";
	_sAttributeFLSWName		= "forwardLimitSwitch";
	_sCommandStateName		= "State";
	_sCommandStatusName		= "Status";
	_sCommandStopName		= "Stop";
	_sCommandForwardName	= "Forward";
	_sCommandBackwardName	= "Backward";	
	_mProxy                 = NULL;
    _bAllowMove             = true;
	
	
	//fake motion proxy, nothing to do
	if(sProxyName == "none/none/none")
		return;
	
	//create proxy to the device
	try
	{
		_mProxy	=	new Tango::DeviceProxyHelper(sProxyName.c_str());
		if(_mProxy==0)
			throw std::bad_alloc();
		else
			_mProxy->get_device_proxy()->ping();				
	}
	catch (const std::bad_alloc&)
	{
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> Memory allocation exception !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("OUT_OF_MEMORY"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			); 		
	}
	catch (Tango::DevFailed& e)
	{
		_mProxy = 0;
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> check the Device proxy Name (" << _sProxyName << ") !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			);
	}
	catch(...)
	{
		_mProxy = 0;
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> check the Device proxy Name (" << _sProxyName << ") !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			); 
	}
}



//////////////////////////////////////////////////////////////////////////////////////
//-ctor
//////////////////////////////////////////////////////////////////////////////////////
MotionProxyHelper::MotionProxyHelper( 	const string sProxyName,
										const string sDescription,
										const string sAttributePositionName ,
										const string sCommandStateName,
										const string sCommandStopName,
										Tango::DeviceImpl * _device)
throw (Tango::DevFailed)
: Tango::LogAdapter((Tango::DeviceImpl *)_device)
{
	DEBUG_STREAM << "MotionProxyHelper::MotionProxyHelper():2 [ "<< sDescription<< " ] entering...  "  << endl;
    yat::MutexLock scoped_lock(_lock);
	//Init. members
	_sProxyName				= sProxyName;
	_sDescription			= sDescription;
	_sAttributePositionName	= sAttributePositionName;
	_sAttributeBLSWName		= "backwardLimitSwitch";
	_sAttributeFLSWName		= "forwardLimitSwitch";
	_sCommandStateName		= sCommandStateName;
	_sCommandStatusName		= "Status";
	_sCommandStopName		= sCommandStopName;
	_sCommandForwardName	= "Forward";
	_sCommandBackwardName	= "Backward";
	_mProxy                 = NULL;
    _bAllowMove             = true;


	//fake motion proxy, nothing to do
	if(sProxyName == "none/none/none")
		return;

	//create proxy to the device
	try
	{
		_mProxy	=	new Tango::DeviceProxyHelper(sProxyName.c_str());
		if(_mProxy==0)
			throw std::bad_alloc();
		else
			_mProxy->get_device_proxy()->ping();
	}
	catch (const std::bad_alloc&)
	{
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> Memory allocation exception !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("OUT_OF_MEMORY"),
			static_cast<const char*>(o.str().c_str()),
			static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			);
	}
	catch (Tango::DevFailed& e)
	{
		_mProxy = 0;
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> check the Device proxy Name (" << _sProxyName << ") !" << endl;
		Tango::Except::re_throw_exception
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			);
	}
	catch(...)
	{
		_mProxy = 0;
		TangoSys_OMemStream o;
		o << "Unable to create proxy to " << _sDescription << " --> check the Device proxy Name (" << _sProxyName << ") !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"),
			static_cast<const char*>(o.str().c_str()),
			static_cast<const char*>("MotionProxyHelper::MotionProxyHelper()")
			);
	}
}


//////////////////////////////////////////////////////////////////////////////////////
//-dtor
//////////////////////////////////////////////////////////////////////////////////////
MotionProxyHelper::~MotionProxyHelper() 
{
	DEBUG_STREAM << "MotionProxyHelper::~MotionProxyHelper() [ "<< _sDescription<< " ] entering...  "  << endl;
    yat::MutexLock scoped_lock(_lock);
	delete _mProxy;
}


//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
void MotionProxyHelper::Move(double dMotorValue)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Move() [ "<< _sDescription<< " ] entering...  " << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->write_attribute(_sAttributePositionName.c_str(),dMotorValue);
        }
        catch (Tango::DevFailed& e) 
        {
            TangoSys_OMemStream o;
            o << "Unable to write attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::re_throw_exception 
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::Move()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to write attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"), 
                static_cast<const char*>(o.str().c_str()), 
                static_cast<const char*>("MotionProxyHelper::Move()")
                ); 
        }
    }
    else
    {
        DEBUG_STREAM << "MotionProxyHelper::Move() (Moving is disabled)" << endl;
    }
}


//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
void MotionProxyHelper::Stop(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Stop() [ "<< _sDescription<< " ] entering...  "  << endl;	
    yat::MutexLock scoped_lock(_lock);
	try
	{
		if(_mProxy!=NULL)
			_mProxy->command(_sCommandStopName.c_str());
	}
	catch (Tango::DevFailed& e)
	{
		TangoSys_OMemStream o;
		o << "Unable to execute command "<<_sCommandStopName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::Stop()")
			);
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Unable to execute command "<<_sCommandStopName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::Stop()")
			); 
	}		
}

//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
void MotionProxyHelper::Forward(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Forward() [ "<< _sDescription<< " ] entering...  " << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->command(_sCommandForwardName.c_str());
        }
        catch (Tango::DevFailed& e)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandForwardName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::re_throw_exception 
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::Forward()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandForwardName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"), 
                static_cast<const char*>(o.str().c_str()), 
                static_cast<const char*>("MotionProxyHelper::Forward()")
                );
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
void MotionProxyHelper::Backward(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Backward() [ "<< _sDescription<< " ] entering...  " << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->command(_sCommandBackwardName.c_str());
        }
        catch (Tango::DevFailed& e)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandBackwardName <<" of the device " << _sDescription << " !" << endl;	
            Tango::Except::re_throw_exception 
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::Backward()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandBackwardName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"), 
                static_cast<const char*>(o.str().c_str()), 
                static_cast<const char*>("MotionProxyHelper::Backward()")
                ); 
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
double MotionProxyHelper::Read(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Read() [ "<< _sDescription<< " ] entering...  "  << endl;
    yat::MutexLock scoped_lock(_lock);
	double dMotorValue=0.0;
	try
	{
		if(_mProxy!=NULL)
			_mProxy->read_attribute(_sAttributePositionName.c_str(),dMotorValue);
	}
	catch (Tango::DevFailed& e) 
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::Read()")
			);
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::Read()")
			); 
	}
	return dMotorValue;
}

//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
double MotionProxyHelper::ReadW(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::ReadW() [ "<< _sDescription<< " ] entering...  "  << endl;		
    yat::MutexLock scoped_lock(_lock);
	double dMotorValue=0.0;
	try
	{
		if(_mProxy!=NULL)
			_mProxy->read_attribute_w(_sAttributePositionName.c_str(),dMotorValue);
	}
	catch (Tango::DevFailed& e) 
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::ReadW()")
			);
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributePositionName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::ReadW()")
			); 
	}
	return dMotorValue;
}

//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
bool MotionProxyHelper::ReadBLSW(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::ReadBLSW() [ "<< _sDescription<< " ] entering...  "  << endl;	
    yat::MutexLock scoped_lock(_lock);
	bool bBLSWValue = false;
	try
	{
		if(_mProxy!=NULL)
			_mProxy->read_attribute(_sAttributeBLSWName.c_str(),bBLSWValue);
	}
	catch (Tango::DevFailed& e)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributeBLSWName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::ReadBLSW()")
			);
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributeBLSWName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::ReadBLSW()")
			); 
	}
	return bBLSWValue;
}

//////////////////////////////////////////////////////////////////////////////////////
//- 
//////////////////////////////////////////////////////////////////////////////////////
bool MotionProxyHelper::ReadFLSW(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::ReadFLSW() [ "<< _sDescription<< " ] entering...  "  << endl;	
    yat::MutexLock scoped_lock(_lock);
	bool bFLSWValue = false;
	try
	{
		if(_mProxy!=NULL)
			_mProxy->read_attribute(_sAttributeFLSWName.c_str(),bFLSWValue);
	}
	catch (Tango::DevFailed& e)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributeFLSWName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception 
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::ReadFLSW()")
			);
	}
	catch(...)
	{
		TangoSys_OMemStream o;
		o << "Unable to read attribute "<<_sAttributeFLSWName <<" of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"), 
			static_cast<const char*>(o.str().c_str()), 
			static_cast<const char*>("MotionProxyHelper::ReadFLSW()")
			); 
	}
	return bFLSWValue;
}


//------------------------------------------------------------------------------
/// return the state of the device
/*!
@return DevState value
*/
//------------------------------------------------------------------------------
Tango::DevState MotionProxyHelper::State(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::State() [ "<< _sDescription<< " ] entering...  " << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
	Tango::DevState MotorState = Tango::STANDBY;

    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->command_out(_sCommandStateName.c_str(),MotorState);
        }
        catch (Tango::DevFailed& e)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandStateName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::re_throw_exception 
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::State()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandStateName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"), 
                static_cast<const char*>(o.str().c_str()), 
                static_cast<const char*>("MotionProxyHelper::State()")
                ); 
        }
    }
	return MotorState;
}


//------------------------------------------------------------------------------
/// return the current status status of the motor
/*!
@return status message string
 */
//------------------------------------------------------------------------------
string MotionProxyHelper::Status(void)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::State() [ "<< _sDescription<< " ] entering...  " << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
	string MotorStatus = "STANDBY";
	
    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->command_out(_sCommandStatusName.c_str(),MotorStatus);
        }
        catch (Tango::DevFailed& e)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandStatusName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::re_throw_exception 
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::Status()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<_sCommandStatusName <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"), 
                static_cast<const char*>(o.str().c_str()), 
                static_cast<const char*>("MotionProxyHelper::Status()")
                ); 
        }
        return MotorStatus;
    }
}


//------------------------------------------------------------------------------
/// Execute a command of the device
//------------------------------------------------------------------------------
void MotionProxyHelper::Command(const string & cmd_name) ///< [in] name of the command to execute
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Command("<<cmd_name<<") [ "<< _sDescription<< " ] entering...  "  << "_bAllowMove: " << _bAllowMove;
    yat::MutexLock scoped_lock(_lock);
    if (_bAllowMove)
    {
        try
        {
            if(_mProxy!=NULL)
                _mProxy->command(cmd_name);
        }
        catch (Tango::DevFailed& e)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<cmd_name <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::re_throw_exception
                ( e,
                  static_cast<const char*>("TANGO_DEVICE_ERROR"),
                  static_cast<const char*>(o.str().c_str()),
                  static_cast<const char*>("MotionProxyHelper::Command()")
                );
        }
        catch(...)
        {
            TangoSys_OMemStream o;
            o << "Unable to execute command "<<cmd_name <<" of the device " << _sDescription << " !" << endl;
            Tango::Except::throw_exception
                (
                static_cast<const char*>("UNKNOWN_ERROR"),
                static_cast<const char*>(o.str().c_str()),
                static_cast<const char*>("MotionProxyHelper::Command()")
                );
        }
    }
}


//------------------------------------------------------------------------------
/// Change the value of _bAllowMove.
/// This boolean controls wether or not any motor will be actually set in motion.
//------------------------------------------------------------------------------
void MotionProxyHelper::AllowMove(const bool bAllowMove)
{
    DEBUG_STREAM << "MotionProxyHelper::[" << _sDescription<< "]::AllowMove(" << bAllowMove << ")";
    yat::MutexLock scoped_lock(_lock);
    _bAllowMove = bAllowMove;
}


}
