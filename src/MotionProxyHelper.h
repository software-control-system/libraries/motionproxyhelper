#ifndef _MotionProxyHelper_H_
#define _MotionProxyHelper_H_
// ============================================================================
//
// = CONTEXT
//		Utility class - Implementation of a class encapsulating creation and control
//						of a Tango device proxy to a standard soleil motor
//
// = File
//		MotionProxyHelper.h
//
// = AUTHOR
//		Arafat NOUREDDINE - Synchrotron SOLEIL
//      F. Langlois - Synchrotron SOLEIL
//
// ============================================================================
#include <tango.h>
#include <TangoExceptionsHelper.h>
#include <DeviceProxyHelper.h>
#include <yat/threading/Mutex.h>

using namespace std;
namespace mph
{
	
class  MotionProxyHelper : public Tango::LogAdapter
{
public:

	//- ctor 
	MotionProxyHelper(	const string sProxyName,
						const string sDescription,
						Tango::DeviceImpl * _device)
	throw (Tango::DevFailed) ;

	//- ctor
	MotionProxyHelper(	const string sProxyName,
						const string sDescription,
						const string sAttributePositionName ,
						const string sCommandStateName,
						const string sCommandStopName,
						Tango::DeviceImpl * _device)
	throw (Tango::DevFailed) ;
	
	//- dtor 
	virtual ~MotionProxyHelper();
	
	//- Move motor to a new position
	virtual void Move(double)
	throw (Tango::DevFailed);

	//- Stop the motor
	virtual void Stop(void)
	throw (Tango::DevFailed);
	
	//- Forward the motor
	virtual void Forward(void)
	throw (Tango::DevFailed);
	
	//- Backward the motor
	virtual void Backward(void)
	throw (Tango::DevFailed);
	
	//- Read the read part of the attribute "attributePositionName" associated to the motor (call read_attribute)
	virtual double Read(void)
	throw (Tango::DevFailed);

	//- Read the write part of the attribute "attributePositionName" associated to the motor (call read_attribute_w)
	virtual double ReadW(void)
	throw (Tango::DevFailed);

	//- Read the the attribute "attributeBLSWName" associated to the motor (call read_attribute)
	bool ReadBLSW(void)
	throw (Tango::DevFailed);
	
	//- Read the the attribute "attributeFLSWName" associated to the motor (call read_attribute)
	bool ReadFLSW(void)
	throw (Tango::DevFailed);

	//- Read the state of the motor
	virtual Tango::DevState State(void)
	throw (Tango::DevFailed);
	
	//- Read the status of the motor
	string Status(void)
	throw (Tango::DevFailed);
    
    void AllowMove(const bool bAllowMove);

	//---------------------------------------------------------------------------------------------------------------
	// generic methods, intented to manipulate any command/attribute
	//---------------------------------------------------------------------------------------------------------------

	//- Execute any simple command without arg
	void Command(const string & cmd)
	throw (Tango::DevFailed);

	//- Execute any simple command with arg
	template <typename T>
	void Command(const string & cmd, T value)
	throw (Tango::DevFailed);

	//- Write on any attribute
	template <typename T>
	void WriteAttribute(const string & cmd, T value)
	throw (Tango::DevFailed);

	//- Read any attribute
	template <typename T>
	T ReadAttribute(const string & cmd)
	throw (Tango::DevFailed);
	

protected:
	Tango::DeviceProxyHelper* 	_mProxy;
	string 					  	_sProxyName;
	string 					  	_sDescription;
	string 					  	_sAttributePositionName;
	string						_sAttributeBLSWName;
	string 						_sAttributeFLSWName;
	string 					  	_sCommandStateName;
	string 					  	_sCommandStatusName;	
	string 					  	_sCommandStopName;
	string 					  	_sCommandForwardName;
	string 					  	_sCommandBackwardName;	
private:	
    bool                        _bAllowMove;
    yat::Mutex			        _lock;    

};

//////////////////////////////////////////////////////////////////////////////////////
//-
//////////////////////////////////////////////////////////////////////////////////////
template <typename T>
void MotionProxyHelper::Command(const string & cmd_name, T arg)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::Command("<<cmd_name<<","<<arg<<") [ "<< _sDescription<< " ] entering...  "  << endl;
	try
	{
		if(_mProxy!=0)
			_mProxy->command_in(cmd_name.c_str(),arg);
	}
	catch (Tango::DevFailed& e)
	{
		//ERROR_STREAM << e << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to execute command "<<cmd_name<<" with arg "<<arg <<" on the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::Command()")
			);
	}
	catch(...)
	{
		//ERROR_STREAM << "Unknown Exception" << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to execute command "<<cmd_name<<" with arg "<<arg <<" on the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"),
			static_cast<const char*>(o.str().c_str()),
			static_cast<const char*>("MotionProxyHelper::Command()")
			);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
//-
//////////////////////////////////////////////////////////////////////////////////////
template <typename T>
void MotionProxyHelper::WriteAttribute(const string & attr_name, T value)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::WriteAttribute("<<attr_name<<","<<value<<") [ "<< _sDescription<< " ] entering...  "  << endl;
	try
	{
		if(_mProxy!=0)
			_mProxy->write_attribute(attr_name.c_str(),value);
	}
	catch (Tango::DevFailed& e)
	{
		//ERROR_STREAM << e << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to write "<<value<<" on "<<attr_name <<"attribute of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::WriteAttribute()")
			);
	}
	catch(...)
	{
		//ERROR_STREAM << "Unknown Exception" << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to write "<<value<<" on "<<attr_name <<" attribute of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"),
			static_cast<const char*>(o.str().c_str()),
			static_cast<const char*>("MotionProxyHelper::WriteAttribute()")
			);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
//-
//////////////////////////////////////////////////////////////////////////////////////
template <typename T>
T MotionProxyHelper::ReadAttribute(const string & attr_name)
throw (Tango::DevFailed)
{
	DEBUG_STREAM << "MotionProxyHelper::ReadAttribute("<<attr_name<<") [ "<< _sDescription<< " ] entering...  "  << endl;
	T value = 0.;
	try
	{
		if(_mProxy!=0)
			_mProxy->read_attribute(attr_name.c_str(),value);
	}
	catch (Tango::DevFailed& e)
	{
		//ERROR_STREAM << e << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to read "<<attr_name <<"attribute of the device " << _sDescription << " !" << endl;
		Tango::Except::re_throw_exception
			( e,
			  static_cast<const char*>("TANGO_DEVICE_ERROR"),
			  static_cast<const char*>(o.str().c_str()),
			  static_cast<const char*>("MotionProxyHelper::ReadAttribute()")
			);
	}
	catch(...)
	{
		//ERROR_STREAM << "Unknown Exception" << ENDLOG;
		TangoSys_OMemStream o;
		o << "Unable to read "<<attr_name <<" attribute of the device " << _sDescription << " !" << endl;
		Tango::Except::throw_exception
			(
			static_cast<const char*>("UNKNOWN_ERROR"),
			static_cast<const char*>(o.str().c_str()),
			static_cast<const char*>("MotionProxyHelper::ReadAttribute()")
			);
	}
	return value;
}
}

////////////////////////////////////////////////////////////////////////////////

#endif // _MotionProxyHelper_H_
