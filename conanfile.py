from conan import ConanFile

class motionproxyhelperRecipe(ConanFile):
    name = "motionproxyhelper"
    version = "1.1.2"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Nourreddine", "Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/motionproxyhelper"
    description = "MotionProxyHelper library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**"

    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("yat/[>=1.0]@soleil/stable")
